import { Component } from '@angular/core';
import {HelloService} from "./service/hello.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  greeting: any = 'Welcome';

  constructor(private helloService: HelloService) { }

  getHello() {
    this.helloService.sayHello().subscribe(response => {
      this.greeting = response["res"];
    });
  }
}
