package com.minigamez.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private final static Logger log = LoggerFactory.getLogger(HelloController.class);

    @GetMapping("/api/hello")
    public ResponseEntity<String> sayHello() {
        log.info("Sending Hello");
        return new ResponseEntity<>("{\"res\":\"Hello\"}", HttpStatus.OK);
    }
}
