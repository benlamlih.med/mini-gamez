package com.minigamez;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinigamezApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinigamezApplication.class, args);
	}

}
