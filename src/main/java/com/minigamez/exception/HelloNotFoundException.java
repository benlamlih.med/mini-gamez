package com.minigamez.exception;

public class HelloNotFoundException extends RuntimeException {
    public HelloNotFoundException(Long id) {
        super("Could not find hello with id: " + id);
    }
}
